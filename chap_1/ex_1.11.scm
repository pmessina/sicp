#!/usr/bin/guile -s
!#

; Recursive Definition
(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1)) (* 2 (f (- n 2))) (* 3 (f (- n 3))))))

; Iterative
; See ex_1.11.c for a c version in a for loop
(define (g n)
  (define (giter i a b c)
    (define new_a (+ a (* 2 b) (* 3 c)))
    (if (= n i)
        new_a 
    (giter (+ i 1) new_a a b)))
  (if (< n 3)
      n
      (giter 3 2 1 0)))

(display (f 10))
(newline)
(newline)
(display (g 10))
(newline)
