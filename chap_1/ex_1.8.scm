#!/usr/bin/guile -s
!#

(define (square x)
  (* x x))

(define (cube x)
  (* x x x))

(define (improve guess x)
  (/
     (+ (/ x (square guess)) (* 2 guess))
     3))

(define (good-enough? guess x)
  (= guess (improve guess x)))

(define (cubert-iter guess x)
  (if (good-enough? guess x)
          guess
          (cubert-iter (improve guess x)
                     x)))

(define (cubert x)
  (cubert-iter 1.0 x))

(define num 100000000000000)
(display (cubert num))
(newline)
(define num 0.000000000000001)
(display (cubert num))
