#!/usr/bin/guile -s
!#

(define (improve guess x)
  (average guess (/ x guess)))

(define (average x y)
  (/ (+ x y) 2))

(define (square x)
  (* x x))

; (define (good-enough? guess x)
;   (< (abs (- (square guess) x)) 0.001))

(define (good-enough? guess x)
  (= guess (improve guess x)))

(define (sqrt-iter guess x)
  (if (good-enough? guess x)
          guess
          (sqrt-iter (improve guess x)
                     x)))

(define (sqrt x)
  (sqrt-iter 1.0 x))

(define num 100000000000000)
(display (sqrt 0.00000000000001))
(display (sqrt num))
(newline)
(display num)
(newline)
(display (square (sqrt num)))

(define num 4)
(newline)
(display (sqrt num))
