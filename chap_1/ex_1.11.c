/*
 * Just using this to help think about the problem. 
 */
#include <stdio.h>

int f(int n) 
{
    if (n < 3)
        return n;

    return f(n-1) + 2 * f(n-2) + 3 * f(n-3);
}


int g(int n) 
{
    int a;
    int old_a;
    int b;
    int old_b;
    int c;
    int i;

    if (n < 3)
        return n;

    /* Initial values are f(3) */
    a = 2;
    b = 1;
    c = 0;

    for (i=3; i <= n; i++) {

        old_a = a;
        old_b = b;

        a = a + 2 * b + 3 * c;
        b = old_a;
        c = old_b;

    }

    return a;
}


int main(void)
{

    printf("%d\n", f(8));
    printf("%d\n", g(8));
    
    return 0;
}
