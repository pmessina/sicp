#!/usr/bin/guile -s
!#

(define (pascal-tri n k)
  (if (or (= k 1) (= k n))
      1
      (+ (pascal-tri (- n 1) (- k 1)) (pascal-tri (- n 1) k))))

(display (pascal-tri 3 2))
(newline)
(display (pascal-tri 5 3))
(newline)
